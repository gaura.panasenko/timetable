# Generated by Django 2.2 on 2019-05-03 10:48

from django.db import migrations, models
import django.db.models.deletion
import yearlessdate.models


class Migration(migrations.Migration):

    dependencies = [
        ('timetableapp', '0014_auto_20190503_1044'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormOfStudySemester',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_range', yearlessdate.models.YearlessDateRangeField(verbose_name='Default time interval')),
                ('form', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='timetableapp.FormOfStudy', verbose_name='Form of study')),
            ],
            options={
                'verbose_name': 'Semester',
                'verbose_name_plural': 'Semesters',
            },
        ),
    ]
