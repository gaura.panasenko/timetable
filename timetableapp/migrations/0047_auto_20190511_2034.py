# Generated by Django 2.2.1 on 2019-05-11 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetableapp', '0046_auto_20190511_2031'),
    ]

    operations = [
        migrations.AddField(
            model_name='curriculum',
            name='end_date',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='end date'),
        ),
        migrations.AddField(
            model_name='curriculum',
            name='start_date',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='start date'),
        ),
        migrations.DeleteModel(
            name='GroupStreamSemester',
        ),
    ]
