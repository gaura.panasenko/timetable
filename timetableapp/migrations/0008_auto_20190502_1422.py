# Generated by Django 2.2 on 2019-05-02 14:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timetableapp', '0007_auto_20190502_1118'),
    ]

    operations = [
        migrations.RenameField(
            model_name='curriculumentry',
            old_name='independentWork',
            new_name='independent_work',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='firstName',
            new_name='first_name',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='lastName',
            new_name='last_name',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='middleName',
            new_name='middle_name',
        ),
        migrations.RenameField(
            model_name='semesterschedule',
            old_name='endDate',
            new_name='end_date',
        ),
        migrations.RenameField(
            model_name='semesterschedule',
            old_name='startDate',
            new_name='start_date',
        ),
        migrations.AlterUniqueTogether(
            name='person',
            unique_together={('first_name', 'middle_name', 'last_name')},
        ),
    ]
