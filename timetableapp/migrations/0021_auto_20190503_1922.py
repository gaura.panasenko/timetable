# Generated by Django 2.2 on 2019-05-03 19:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('timetableapp', '0020_auto_20190503_1918'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='group',
            options={'verbose_name': 'Group', 'verbose_name_plural': 'Groups'},
        ),
        migrations.AlterUniqueTogether(
            name='group',
            unique_together={('parent', 'number')},
        ),
    ]
