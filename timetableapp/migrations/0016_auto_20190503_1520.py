# Generated by Django 2.2 on 2019-05-03 15:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('timetableapp', '0015_formofstudysemester'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SemesterSchedule',
            new_name='GroupSemester',
        ),
        migrations.AlterModelOptions(
            name='formofstudysemester',
            options={'verbose_name': 'Semester time interval', 'verbose_name_plural': 'Semester time intervals'},
        ),
        migrations.AlterField(
            model_name='formofstudy',
            name='semesters',
            field=models.PositiveSmallIntegerField(default=8, help_text='Total number of semesters, later if you specify fewer Semester time intervals than Number of semesters they will cycle from beginning automatically', verbose_name='Number of semesters'),
        ),
    ]
